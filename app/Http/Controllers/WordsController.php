<?php

namespace App\Http\Controllers;

use App\Translation;
use App\Word;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Http\Requests;

class WordsController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('words.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
/*
        $words = Word::join(DB::raw('(SELECT word_id, MAX(created_at) as created_at, updated_at, MAX(status) as status FROM translations GROUP BY word_id) as lastTranslations'), function ($join) {
            $join->on('lastTranslations.word_id', '=', 'words.id');
        })->select(['words.id', 'words.word', 'words.language', 'lastTranslations.status', 'lastTranslations.created_at', 'lastTranslations.updated_at']);
*/

        $words = DB::table(DB::raw("(SELECT id, word, language, created_at, updated_at,(select count(*) FROM translations WHERE word_id=words.id AND status <> 2) as history_count, (select status from translations where word_id=words.id AND status <> 2 ORDER by updated_at DESC LIMIT 1) as status FROM words) as words"));

        return Datatables::of($words)
            ->editColumn('id', function ($word) {
                return '<a  href="/word/' . $word->id . '">' . $word->id . '</a>';
            })
            ->editColumn('word', function ($word) {
                return '<a  href="/word/' . $word->id . '">' . $word->word . '</a>';
            })
            ->editColumn('status', function ($translation) {
                return isset($translation->status) ? Translation::$statuses[$translation->status] : '';
            })
            ->editColumn('language', function ($word) {
                return $word->language == Word::LANG_QIRIM ? 'QT' : 'RU';
            })
            ->make(true);
    }

    public function show($word)
    {
        return view('words.edit', ['word' => $word]);
    }

    public function delete($word)
    {
        $word->delete();

        return redirect()->action(
            'WordsController@getIndex'
        );
    }


    public function wordAddAdmin(Request $request)
    {
        $word = Word::firstOrCreate(['word' => $request->word, 'language' => $request->language]);


        return redirect()->action(
            'WordsController@show', ['word' => $word]
        );
    }

    public function add(Request $request)
    {

        if(!$request->word || !$request->translation) {
            return response()->json([]);
        }

        if((int)$request->id > 0) {
            $word = Word::find((int)$request->id);
        }
        elseif (trim($request->word) != '') {
           $word = Word::firstOrCreate(['word' => $request->word, 'language' => $request->language]);
        }

        $word->translations()->create([
            'body' => $request->input('translation'),
            'status' => Translation::WORD_STATUS_PROPOSED
        ]);

        return response()->json($word);
    }

    public function AddTranslation($word, Request $request)
    {
        $word->translations()->create([
            'body' => $request->input('body'),
            'status' => Translation::WORD_STATUS_PROPOSED
        ]);

        return redirect()->action(
            'WordsController@show', ['word' => $word]
        );
    }

    public function index($language, $offset = 0, $limit = 100)
    {
        $words = Word::where('language', $language)->where('status', Word::WORD_STATUS_APPROVED)->skip($offset)->take($limit)->get();

        return response()->json($words);
    }

    public function search($word)
    {
        $word = Word::where('word', mb_strtolower($word))->where('status', Word::WORD_STATUS_APPROVED)->get();

        return response()->json($word);
    }

    public function lastAdded($date = '1970-01-01 00:00:00')
    {

        if ($date == '1970-01-01 00:00:00') {
            return redirect()->to('/init/words.json');
        }

        $words = [];

        foreach (Word::where('updated_at', '>', $date)->get() as $word) {

            if(!$translation = $word->translations->where('status', Translation::WORD_STATUS_APPROVED)->first())
                continue;

            $output = $word;
            $output->translation = $translation->body;
            $output->status = Translation::WORD_STATUS_APPROVED;
            unset($output->translations);

            $words[] = $output;

        }

        return response()->json($words);
    }
}
