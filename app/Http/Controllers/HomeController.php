<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Jobs\SendPushNotification;
use App\Token;
use App\Translation;
use App\Word;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function wordsImportShow()
    {
        return view('words.import');
    }

    private function import()
    {

    }

    public function upload(Request $request)
    {


        $request->file('importfile')->move(storage_path(), 'import.csv');

        $fp = fopen(storage_path() . '/' . 'import.csv', 'r');

        while ($row = fgetcsv($fp, 1024, ';')) {
            //var_dump($row);

            $word = trim($row[0]);
            $lang = trim($row[1]);
            $trans = trim($row[2]);

            $added = 0;
            $updated = 0;

            if ($word && $lang && $trans) {
                if ($wordObj = Word::where('word', $word)->where('language', $lang)->first()) {
                    $status = Translation::WORD_STATUS_PROPOSED;
                    $updated++;
                } else {
                    $status = Translation::WORD_STATUS_APPROVED;
                    $wordObj = Word::create(['word' => $word, 'language' => $lang]);
                    $added++;
                }

                $wordObj->translations()->create([
                    'body' => $trans,
                    'status' => $status
                ]);
            }
        }

        Session::flash('added', $added);
        Session::flash('updated', $updated);
        return Redirect::to('/import');

    }


}
