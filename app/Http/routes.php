<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => ['auth']], function() {
    Route::controller('words', 'WordsController', [
        'anyData'  => 'words.data',
        'getIndex' => 'words',
    ]);
    Route::get('word/{word}', 'WordsController@show');
    Route::get('word/{word}/delete', 'WordsController@delete');
    Route::post('word/add', 'WordsController@wordAddAdmin');
    Route::post('word/{word}/translation', 'WordsController@AddTranslation');

    Route::get('translation/{translation}/approve', 'TranslationsController@approve');
    Route::get('translation/{translation}/disapprove', 'TranslationsController@disapprove');
    Route::get('translation/{translation}/delete', 'TranslationsController@delete');

    Route::get('wordlist/{language}/{offset}/{limit}', 'WordsController@index');
    Route::get('wordsearch/{word}', 'WordsController@search');

    Route::controller('announcements', 'AnnouncementsController', [
        'anyData'  => 'announcements.data',
        'getIndex' => 'announcements',
    ]);
    Route::post('announcement', 'AnnouncementsController@add');

    //API: загрузка клиентских изображений
    Route::post('photos', array('as' => 'savePhotos', 'uses' => 'AnnouncementsController@savePhotos'));

    Route::get('/home', 'HomeController@index');
    Route::get('/import', 'HomeController@wordsImportShow');
    Route::post('/upload', 'HomeController@upload');


});

Route::post('token', 'TokenController@add');
Route::post('word', 'WordsController@add');
Route::get('last-added/{date?}', 'WordsController@lastAdded');

Route::get('announcementlist', 'AnnouncementsController@index');
Route::get('announcement/{id}', 'AnnouncementsController@show');

Route::auth();


