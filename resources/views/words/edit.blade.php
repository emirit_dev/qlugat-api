@extends('layouts.master')

@section('content')
    <h1><a href="/words">&larr;</a> {{ $word->word }} <a title="Удалить" style="color: red" onclick="return confirm('точно?')" href="/word/{{ $word->id }}/delete">&times;</a></h1>


    <h4>История версий переводов</h4>
    <table class="table table-bordered">
        <thead>
            <th>Перевод</th>
            <th>Предложен</th>
            <th>Статус</th>
            <th>Публикация</th>
            <th>Удалить</th>
        </thead>

        <tbody>
            @foreach($word->translations as $translation)
                @if($translation->status != \App\Translation::WORD_STATUS_DISABLED)
                <tr @if($translation->status == \App\Translation::WORD_STATUS_APPROVED)style="background-color: #99cb84"@endif>
                    <td>{{ $translation->body }}</td>
                    <td>{{ $translation->updated_at }}</td>
                    <td>@if($translation->status != \App\Translation::WORD_STATUS_APPROVED)Черновик@else Опубликовано @endif</td>
                    <td>@if($translation->status != \App\Translation::WORD_STATUS_APPROVED)<a class="btn btn-default" href="/translation/{{ $translation->id }}/approve">опубликовать</a> @endif</td>
                    <td>@if($translation->status != \App\Translation::WORD_STATUS_APPROVED)<a class="btn" style="color: red" href="/translation/{{ $translation->id }}/delete">&times;</a> @endif</td>
                </tr>
                @endif
            @endforeach
            {!! Form::open(['url' => '/word/' . $word->id . '/translation', 'method' => 'post']) !!}
                <tr>
                    <td><textarea name="body" style="width: 100%" rows="10" cols="10"></textarea></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button type="submit" class="btn">Добавить версию</button></td>
                    <td>&nbsp;</td>
                </tr>
            {!! Form::close() !!}
        </tbody>

    </table>



@stop
