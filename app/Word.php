<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    //

    const LANG_QIRIM = 'Q';
    const LANG_RUSSIAN = 'R';

    protected $fillable = [
        'word', 'language'
    ];

    public function translations()
    {
        return $this->hasMany('App\Translation');
    }
}
