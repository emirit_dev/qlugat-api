@extends('layouts.master')

@section('content')
    {!! Form::open(['url' => '/word/add', 'method' => 'post', 'class' => 'add-word well']) !!}
    <h4>Добавить слово </h4>
    <p>
        <label>Слово</label>
        <input type="text" class="form-control" name="word">
    </p>
    <p>
        <label>Язык</label>
        <select class="form-control" name="language">
            <option value="{{ \App\Word::LANG_QIRIM }}">Крымскотатарский</option>
            <option value="{{ \App\Word::LANG_RUSSIAN }}">Русский</option>
        </select>
    </p>

    <p>
        <button type="submit" class="btn btn-primary">Добавить</button>
    </p>
    {!! Form::close() !!}

    <table class="table table-bordered" id="words-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Слово</th>
            <th>Язык</th>
            <!--th>Перевод</th-->
            <!--th>Добавлено</th-->
            <th>Последние изменения</th>
            <th>Статус</th>
            <th>Переводов</th>


        </tr>
        </thead>
        <tfoot></tfoot>
    </table>
@stop

@push('scripts')
<script>
    $(function () {
        $('#words-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('words.data') !!}',
            columns: [
                {data: 'id'},
                {data: 'word'},
                {data: 'language', "searchable":     false},
                //{data: 'created_at', name: 'lastTranslations.created_at'},
                {data: 'updated_at'},
                {data: 'status'},
                {data: 'history_count',  "searchable":     false}
            ],
            order: [[ 3, "desc" ]],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    });




</script>
@endpush


